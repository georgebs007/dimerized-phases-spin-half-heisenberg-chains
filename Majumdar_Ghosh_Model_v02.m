clearvars
tic
N = 6:2:10; %Numer of spins in each system
alpha = 0.35;   %Ratio of J2/J1

spin_label = cell(1,length(N)); %Defines label of each data point
delta = zeros(1,length(N)); %Define array to store energy gap
evecs = cell(1,length(N));  %Cell to store all eigenvectors
evals = cell(1,length(N));  %Cell to store all eigenvalues
gnd_E = zeros(1,length(N)); %Array to store ground state energies
T = cell(1,length(N));  %Cell to store translatoion operator for each system
H = cell(1,length(N));  %Cell to store Hamiltonians of each system for convenience
k = cell(1,length(N));  %Cell to store momentum quantum numbers for each system

%Runs MG model and calculates energy gap
for syst = 1:length(N)
    [evecs{syst},E, H{syst}] = MG_J1J2(N(syst),alpha); %Calls function below to run MG model
    evals{syst} = diag(E);  %Stores eigenvalues in cell defined earlier
    T{syst} = T_op(N(syst));
    
    if evals{syst}(2) - evals{syst}(1) < abs(10^-12)
        %Degenerate ground state case
        delta(syst) = evals{syst}(3) - evals{syst}(1);
        dim_singlet = [0 1 -1 0]'/sqrt(2);
        d_c = spin_cell(dim_singlet,dim_singlet,1,N(syst)/2);   %Temporary cell
        %Creates the two product states
        prod1 = superkron(d_c);
        prod2 = T{syst}*prod1;
        %Finds combination of eigenstates that are also eigenstates of T
        C1 = linsolve([evecs{syst}(:,1),evecs{syst}(:,2)],prod1);
        C2 = linsolve([evecs{syst}(:,1),evecs{syst}(:,2)],prod2);
        %Calculates the even and odd parity eigenstates of T
        eig_T1 = ((C1(1)+C2(1))*evecs{syst}(:,1) + (C1(2)+C2(2))*evecs{syst}(:,2))/sqrt((C1(1)+C2(1))^2+(C1(2)+C2(2))^2);
        eig_T2 = ((C1(1)-C2(1))*evecs{syst}(:,1) + (C1(2)-C2(2))*evecs{syst}(:,2))/sqrt((C1(1)+C2(1))^2+(C1(2)+C2(2))^2);
        %Removes computational error by setting tiny values to 0
        eig_T1(abs(eig_T1)<10^-13) = 0;
        eig_T2(abs(eig_T2)<10^-13) = 0;
        %Calculates crystal momentum for the two eigenstates
        k{syst}(1) = real(log(max((T{syst}*eig_T1)./eig_T1))/1i);
        k{syst}(2) = real(log(max((T{syst}*eig_T2)./eig_T2))/1i);
        
    else
        %Non degenerate ground state
        delta(syst) = evals{syst}(2) - evals{syst}(1);
        %Eigenstates of first two states are already eigenstates of T
        eig_T1 = evecs{syst}(:,1);
        eig_T2 = evecs{syst}(:,2);
        %Removes computational error by setting tiny values to 0
        eig_T1(abs(eig_T1)<10^-13) = 0;
        eig_T2(abs(eig_T2)<10^-13) = 0;
        %Calculates crystal momentum for the two eigenstates
        k{syst}(1) = real(log(max((T{syst}*eig_T1)./eig_T1))/1i);
        k{syst}(2) = real(log(max((T{syst}*eig_T2)./eig_T2))/1i);
        
    end
    
    gnd_E(syst) = min(diag(E));
    spin_label{syst} = sprintf('  N=%d',N(syst));
    
end
%{
%Plots
set(figure(1),'windowstyle','docked');clf
hold on
plot(N,delta, 'b','DisplayName', '\Delta Against N')
scatter(N,delta,50,'bo')
xlabel('N')
ylabel('\Delta/J')
rg = range(delta);
axis([N(1)-range(N)*0.05, N(end)+range(N)*0.05, min(delta)-0.1*rg, max(delta)+0.1*rg])
axis square
ax1 = gca; % current axes
ax1.XColor = 'b';
ax1.YColor = 'k';

ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,'XAxisLocation','top','YAxisLocation','right','Color','none');
ax2.XColor = 'r';
ax2.YColor = 'k';

x = 1./N;
hold on
axis([min(x)-range(x)*0.2, max(x)+range(x)*0.2, min(delta)-0.1*rg, max(delta)+0.1*rg])
plot(x,delta,'Parent',ax2,'Color','r','DisplayName', '\Delta Against 1/N')
scatter(x,delta,50,'ro')
text(x,delta,spin_label)
ax2.YAxis.Visible = 'off';
xlabel('1/N')
axis square
title('Energy Gap Between Ground and First Excited State in the MG Model')


set(figure(2),'windowstyle','docked');clf
plot(log(N),log(delta),'b','DisplayName','log(\Delta)')
axis square
xlabel('ln(N)')
ylabel('ln(\Delta)')
title('Log Plot of Energy Gap')
dat = fitlm(log(N),log(delta)); %Calculates gradient of line of best fit
text(max(log(N))-range(log(N))/2,min(log(delta))+range(log(delta))/2,sprintf('   Gradient = %f',dat.Coefficients{2,1}))

set(figure(3),'windowstyle','docked');clf
hold on
axis([min(N)-1,max(N)+1,-0.5,1.5])
%Plots k for each spin
for spin = 1:length(N)
    %Checks whether even or odd parity, or ground or first excited state
    if k{spin}(1) == 0
        
        if spin == 1
            p = scatter(N(spin),k{spin}(1)/pi(),100,'bo','filled');
            q = scatter(N(spin),k{spin}(2)/pi(),100,'bx');
            
        else
            scatter(N(spin),k{spin}(1)/pi(),100,'bo','filled');
            scatter(N(spin),k{spin}(2)/pi(),100,'bx');
        end        
    else
        if spin == 1
            p = scatter(N(spin),k{spin}(2)/pi(),100,'bo','filled');
            q = scatter(N(spin),k{spin}(1)/pi(),100,'bx');
            
        else
            scatter(N(spin),k{spin}(2)/pi(),100,'bo','filled');
            scatter(N(spin),k{spin}(1)/pi(),100,'bx');
        end   
        
    end
       
    
end

xlabel('N')
ylabel('k/\pi')
if evals{1}(2) - evals{1}(1) < abs(10^-12)
    title('Crystal Momentum of Degenerate Ground States')
    legend([p q],{'Even Parity','Odd Parity'},'location','north')
else
    title('Crystal Momentum of First Two Eigenstates')
    legend([p q],{'Ground State','First Excited State'},'location','north')
end


%Cycling through spins to plot energy levels of each spin
for spin = 1:length(N)
    set(figure(3+spin),'windowstyle','docked');clf
    hold on
    %RGB triplets to colour the plots
    r = linspace(0,256,length(evals{spin}));
    g = [linspace(0,256,length(evals{spin})/2),linspace(256,0,length(evals{spin})/2)];
    b = linspace(256,0,length(evals{spin}));
    c = transpose([r;g;b])/256;
    
    axis([-0.25 1.25 -5 5])
    ax = gca;
    ax.XAxis.Visible = 'off';
    ylabel('E/J')
    
    if evals{spin}(2) - evals{spin}(1) < abs(10^-12)
        title(sprintf('Energy Levels of MG System With %d Spins',N(spin)))
    else
        title(sprintf('Energy Levels of Non-Degenerate System With %d Spins',N(spin)))
    end
    
    
    text(0.5,min(evals{spin})-0.5,sprintf('\\alpha=%g',alpha))
    
    for eigenvalue = 1:length(evals{spin})
        plot([0 1],[evals{spin}(eigenvalue),evals{spin}(eigenvalue)],'Color',c(eigenvalue,:))
    end
    text(1.05,evals{spin}(1)+(delta(spin))/2,sprintf('\\Delta=%.2g',delta(spin)))
    
end
%}
toc

function [evecs,evals,H] = MG_J1J2(N,alpha)

    spin_ops = cell(3,N);   %Stores spin operator in each axis for each spin in cell array

    %Define spin matrices for one spin
    P0 = eye(2);
    Px = [[0 1];[1 0]];
    Py = [[0 -1i];[1i 0]];
    Pz = [[1 0];[0 -1]];
    Sx = Px/2;
    Sy = Py/2;
    Sz = Pz/2;

    %Calculates spin operators in the Hilbert space of system
    for spin = 1:N
            %Superkron neatens tensor product
            spin_ops{1,spin} = superkron(spin_cell(Sx,P0,spin,N));
            spin_ops{2,spin} = superkron(spin_cell(Sy,P0,spin,N));
            spin_ops{3,spin} = superkron(spin_cell(Sz,P0,spin,N));
    end

    %Defines matrix of hamiltonian
    H = zeros(2^N);

    j = [1:N 1 2];  %Array of indices to include periodic boundary conditions
    %Cycles through each spin and adds the exchange term for NN and NNN
    %interactions to the hamiltonian
    for i = 1:N

       SiSi1 = spin_ops{1,j(i)}*spin_ops{1,j(i+1)}+spin_ops{2,j(i)}*spin_ops{2,j(i+1)}+spin_ops{3,j(i)}*spin_ops{3,j(i+1)};
       SiSi2 = spin_ops{1,j(i)}*spin_ops{1,j(i+2)}+spin_ops{2,j(i)}*spin_ops{2,j(i+2)}+spin_ops{3,j(i)}*spin_ops{3,j(i+2)};
       H = H + SiSi1 + alpha*SiSi2; %NN and NNN exchange terms

    end

    %Calculates eigenvalues of H
    [evecs,evals] = eig(H);
    
    
end

%Create cell with each component of the tensor product
%Parameter A: spin operator, B: P0 matrix, pos: index of spin, N: number of spins
function res = spin_cell(A,B,pos,N)
res = cell(1,N);    %Defines cell
    %Cycles through each position in cell
    for position = 1:N
        %Checks whether to insert spin operator or P0
        if position == pos
            res{position} = A;
        else
            res{position} = B;
        end
    end
end

%Calculates Translation operator
function res = T_op(N)
    basis = eye(2^N); %Product basis = identity with dimension 2^N
    dec_rep = 0:2^N-1;  %Decimal representaion of each basis state
    b_rep = dec2bin(dec_rep,N); %Converts to binary with 0 representing spin up and 1 spin down
    b_rep = b_rep(:,[N,1:N-1]); %Translates in binary representation
    T_dec = bin2dec(b_rep)+1;   %Converts back to decimal
    res = basis(:,T_dec');  %Rearranges basis according to the above
end