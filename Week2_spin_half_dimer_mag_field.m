clearvars
B = 0:0.01:10;  %B field in T
g = -2.002; %g factor for an electron
J = 1/400;  %Heisenberg coupling factor in eV using conversion 1eV = 10000K
bohr_mag = 5.788*10^(-5);   %Bohr magnetron in eV
%Energy Levels of spin half dimer in magnetic field
E1 = (-3*J/4)*ones(1,length(B))*1000;    
E2 = ((J/4)-g*bohr_mag*B)*1000;
E3 = (J/4)*ones(1,length(B))*1000;
E4 = ((J/4)+g*bohr_mag*B)*1000;

%Plot of Spectrum
set(figure(1),'windowstyle','docked');clf
hold on
plot(B,E1,'DisplayName','E1')
plot(B,E2,'DisplayName','E2')
plot(B,E3,'DisplayName','E3')
plot(B,E4,'DisplayName','E4')
xlabel('Magnetic Field Strength/T')
ylabel('Energy Spectrum/\mueV')
title('Energy Spectrum of Spin-1/2 Dimer in Magnetic Field')
legend('show','location','northwest')
axis square

txt = sprintf('J = %gK',J*10000);
text(range(B)*0.25,max([E1 E2 E3 E4]),txt)