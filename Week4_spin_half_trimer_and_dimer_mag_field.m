clearvars
B = 0:0.01:10;  %B field in T
g = -2.002; %g factor for an electron
J = 1/400;  %Heisenberg coupling factor in eV using conversion 1eV = 10000K
bohr_mag = 5.788*10^(-5);   %Bohr magnetron in eV

%Energy Levels of triangle, multiplied by 1000 to covert to micro eV
E1 = ((-3*J/4) - g*bohr_mag*B/2)*1000;
E2 = ((-3*J/4) + g*bohr_mag*B/2)*1000;
E3 = ((3*J/4) - g*bohr_mag*B*3/2)*1000;
E4 = ((3*J/4) + g*bohr_mag*B*3/2)*1000;
E5 = ((3*J/4) - g*bohr_mag*B/2)*1000;
E6 = ((3*J/4) + g*bohr_mag*B/2)*1000;

%Energy Levels of tetramer, multiplied by 1000 to convert to micro eV
E1T = (J-2*g*bohr_mag*B)*1000;
E2T = (J-g*bohr_mag*B)*1000;
E3T = (J*ones(1,length(B)))*1000;
E4T = (J+g*bohr_mag*B)*1000;
E5T = (J+2*g*bohr_mag*B)*1000;
E6T = (-J-g*bohr_mag*B)*1000;
E7T = (-J*ones(1,length(B)))*1000;
E8T = (-J+g*bohr_mag*B)*1000;
E9T = (-2*J)*ones(1,length(B))*1000;
E10T = (-g*bohr_mag*B)*1000;
E11T = zeros(1,length(B));
E12T = (g*bohr_mag*B)*1000;

%Plot of Spectrum
set(figure(1),'windowstyle','docked');clf
hold on
plot(B,E1,'DisplayName','E1')
plot(B,E2,'DisplayName','E2')
plot(B,E3,'DisplayName','E3')
plot(B,E4,'DisplayName','E4')
plot(B,E5,'DisplayName','E5')
plot(B,E6,'DisplayName','E6')
xlabel('Magnetic Field Strength/T')
ylabel('Energy Spectrum/\mueV')
title('Energy Spectrum of Spin-1/2 Triangle in Magnetic Field')
legend('show','location','northwest')
axis square

%Range of all values to format plot
rg = [E1 E2 E3 E4 E5 E6];
ylim([min(rg)-0.1*range(rg),max(rg)+0.1*range(rg)]);
txt = sprintf('J = %gK',J*10000);
text(range(B)*0.25,max(rg),txt)


set(figure(2),'windowstyle','docked');clf
hold on
plot(B,E1T,'DisplayName','E1T')
plot(B,E2T,'DisplayName','E2T')
plot(B,E3T,'DisplayName','E3T')
plot(B,E4T,'DisplayName','E4T')
plot(B,E5T,'DisplayName','E5T')
plot(B,E6T,'DisplayName','E6T')
plot(B,E7T,'DisplayName','E7T')
plot(B,E8T,'DisplayName','E8T')
plot(B,E9T,'DisplayName','E9T')
plot(B,E10T,'DisplayName','E10T')
plot(B,E11T,'DisplayName','E11T')
plot(B,E12T,'DisplayName','E12T')
xlabel('Magnetic Field Strength/T')
ylabel('Energy Spectrum/\mueV')
title('Energy Spectrum of Spin-1/2 Tetramer in Magnetic Field')

%Range of all values to format plot
rgT = [E1T E2T E3T E4T E5T E6T E7T E8T E9T E10T E11T E12T];
ylim([min(rgT)-0.1*range(rgT),max(rgT)+0.1*range(rgT)]);
axis square
txt = sprintf('J = %gK',J*10000);
text(range(B)*0.25,max(rgT),txt)