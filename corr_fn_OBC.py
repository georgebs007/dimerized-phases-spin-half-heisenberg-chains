from tenpy.networks.mps import MPS
from tenpy.models.lattice import Chain
from tenpy.models.spins_nnn import SpinChainNNN2
from tenpy.algorithms import dmrg
from tenpy.algorithms.exact_diag import ExactDiag
import numpy as np
import matplotlib.pyplot as plt
import math

#Function to perform least squares regression
def lin_reg(x, y):
    n = len(x)
    xsq = [0]*len(x)
    xy = [0]*len(x)
    for i in range(len(x)):
        xsq[i] = x[i]**2
        xy[i] = x[i]*y[i]
    SXX = sum(xsq) - ((sum(x)**2)/n)
    SXY = sum(xy)-(sum(x)*sum(y)/n)
    b1 = SXY/SXX
    b0 = (sum(y)/n) - (b1*(sum(x)/n))
    
    return [b1,b0]

#Stores the values of J2/J1 to be modelled
alp_store = [0.46,0.47,0.48,0.49]

for val in range(len(alp_store)):
    N = 100  # Number of sites
    alp = alp_store[val]
    B = 0   #Magnetic field
    model = SpinChainNNN2({"S":0.5,"L": N, "Jxp":alp, "Jyp":alp, "Jzp":alp,"hx":B,"hy":B,"hz":B,"bc_x": "open", "bc_MPS": "finite"}) #Define J1J2
    sites = model.lat.mps_sites()
    psi = MPS.from_product_state(sites, ['up','down'] * (N//2), "finite")   #Initial guess of wavefunction


    dmrg_params = {"trunc_params": {"chi_max": 100, "svd_min": 1.e-10}, "mixer": True}
    eng = dmrg.TwoSiteDMRGEngine(psi, model, dmrg_params)
    E, psi = eng.run() # the main work; modifies psi in place
   
    print("E =", E)
    print("max. bond dimension =", max(psi.chi))
    


    #i0 = psi.L // 4  # for fixed `i`
    i0 = 0   #Initial site
    j = np.arange(i0+1, psi.L)   #Range of sites for correlation fn
    #Correlation fn terms using S+, S-, Sz
    col1 = psi.term_correlation_function_right([("Sp", 0)], [("Sm", 0)], i_L=i0, j_R=j)
    col2 = psi.term_correlation_function_right([("Sm", 0)], [("Sp", 0)], i_L=i0, j_R=j)
    col3 = psi.term_correlation_function_right([("Sz", 0)], [("Sz", 0)], i_L=i0, j_R=j)
    col_fn = ((col1 +col2)/2) + col3
    
    #Select section of data to calculate lsr with
    dat_j = [0]*int((len(j)//(10/9)))
    dat_cl = [0]*int((len(j)//(10/9)))
    for ind in range(int((len(j)//(10/9)))):
        dat_j[ind] = abs(j[ind+1])
        dat_cl[ind] = abs(col_fn[ind+1])

    #Calculate lsr for the loglog and loglin
    loglogreg = lin_reg(np.log(dat_j),np.log(dat_cl))
    loglinreg = lin_reg(dat_j,np.log(dat_cl))
    #Data for line of best fit
    x = [j[0],j[len(j)-1]]
    xlog = np.log(x)
    yloglog_lbf = np.exp([el*loglogreg[0]+loglogreg[1] for el in xlog])
    yloglin_lbf = np.exp([el*loglinreg[0]+loglinreg[1] for el in x])
    
    #Critical exponent and correlation length calculated from gradient of plot
    nu = -loglogreg[0]
    col_len = -1/loglinreg[0]

    #Saves all the data and parameters going into the model
    datx = np.concatenate(([N,alp,nu,col_len],j))
    datcol = np.concatenate(([N,alp,nu,col_len],col_fn))
    sav_dat = np.column_stack((datx,datcol))
    alpstr = str(alp)
    alpstr = alpstr.replace('.','')
    np.savetxt("Anaconda\G_Examples\\alp"+alpstr+"N"+str(N)+"OBC.dat", sav_dat)


    #Generates plots
    fig, (ax1,ax2,ax3) = plt.subplots(1, 3)
    fig.suptitle('Correlation Function Against Distance for N = '+str(N)+r', $\alpha$ = ' + str(alp))
    ax1.plot(j, abs(col_fn), label="Cor Funct")
    ax2.plot(j, abs(col_fn), label="Cor Funct")
    ax2.plot(np.exp(xlog),yloglog_lbf)
    ax2.text(j[int(len(j)-1)]/2,abs(col_fn[2]),r'$\nu$: '+str(round(nu,3)))
    ax3.plot(j, abs(col_fn), label="Cor Funct")
    ax3.plot(x,yloglin_lbf)
    ax3.text(j[int(len(j)-1)]/2,abs(col_fn[2]),'l: '+str(round(col_len,3)))
    ax2.loglog()
    ax3.set_yscale("log")
    ax1.set(xlabel=r"$|j-1|$", ylabel=r'$\langle S_1 S_j\rangle$')
    ax2.set(xlabel=r"$|j-1|$")
    ax3.set(xlabel=r"$|j-1|$")
    ax1.set_title('Standard Axes')
    ax2.set_title('log-log')
    ax3.set_title('log-lin')


    fig.set_size_inches(24, 10)

    plt.savefig("Anaconda\G_Examples\\alp"+alpstr+"N"+str(N)+"OBCPlot.jpg", dpi=100)
    #plt.show()



#Exactly diagonalises for comparison
#ED=ExactDiag(model, charge_sector=None, sparse=False, max_size=2000000.0)
#ED.build_full_H_from_mpo()
#ED.full_diagonalization()
#a,b = ED.groundstate()
#print("Groundstate from ED = ", a)
