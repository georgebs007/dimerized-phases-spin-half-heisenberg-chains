clearvars

%Setting up Hilbert Space and spin operators
P0 = eye(2);
Px = [[0 1];[1 0]];
Py = [[0 -1i];[1i 0]];
Pz = [[1 0];[0 -1]];
Sx = Px/2;
Sy = Py/2;
Sz = Pz/2;
S1x = superkron(Sx,P0,P0);
S2x = superkron(P0,Sx,P0);
S3x = superkron(P0,P0,Sx);
S1y = superkron(Sy,P0,P0);
S2y = superkron(P0,Sy,P0);
S3y = superkron(P0,P0,Sy);
S1z = superkron(Sz,P0,P0);
S2z = superkron(P0,Sz,P0);
S3z = superkron(P0,P0,Sz);
SZ = S1z + S2z + S3z;
SS = (S1x+S2x+S3x)^2 + (S1y+S2y+S3y)^2 + (S1z+S2z+S3z)^2;

%Function which contains hamiltonian matrix with input parameters J and gamma*B
H_Tri = @(J,gamma_B) J*(SS - (S1x^2 + S1y^2 + S1z^2) - (S2x^2 + S2y^2 + S2z^2) - (S3x^2 + S3y^2 + S3z^2))/2 - gamma_B*(S1z+S2z+S3z);

%Range of gamma*B/J values
gamma_B_J = -5:0.01:5;

%Matrix which stores energy eigenvalues for each value of gamma*B/J
E = zeros(size(SS,1),length(gamma_B_J));

%Defines matrix to store ground states
gnd_states = zeros(size(SS,1),length(gamma_B_J));

%Define matrix to store expectation values
exp_SZ = zeros(length(gamma_B_J),1);


%Cycles through gamma_B_J and calculates eigenvalues and <Sz>
for k = 1:length(gamma_B_J)
   
    [evecs,evals] = eig(H_Tri(1,gamma_B_J(k)));
    E(:,k) = diag(evals);   %Assigns eigenvales to E
    [gnd_E,I] = min(E(:,k));    %Finds index of ground state energy
    gnd_states(:,k) = evecs(:,I);   %Stores ground state
    exp_SZ(k) = gnd_states(:,k)'*SZ*gnd_states(:,k);    %Calculates expectation value
    
end

%Plots
set(figure(1),'windowstyle','docked');clf
hold on
for eigenvalue = 1:size(E,1)
   plot(gamma_B_J,E(eigenvalue,:),'DisplayName',sprintf('E%d',eigenvalue))
end
legend('show','location','north')
axis square
xlabel('\gammaB/J')
ylabel('E/J')
title('AF Spin 1/2 Triangle in Magnetic Field')


%Plots
set(figure(2),'windowstyle','docked');clf
hold on
axis square
xlabel('\gammaB/J')
ylabel('<Sz>')
title('AF Spin 1/2 Triangle Ground State <Sz>')
scatter(gamma_B_J,exp_SZ, 5,'s', 'red','filled')
axis(1.1*[min(gamma_B_J),max(gamma_B_J),min(exp_SZ(:)),max(exp_SZ(:))])