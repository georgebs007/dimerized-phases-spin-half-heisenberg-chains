clearvars
tic
N = 12;  %Number of spins
J = 1;
alpha = 0.5;    %Ratio J2/J1, 0.5 for MG model

spin_ops = cell(3,N);   %Stores spin operator in each axis for each spin in cell array

%Define spin matrices for one spin
P0 = eye(2);
Px = [[0 1];[1 0]];
Py = [[0 -1i];[1i 0]];
Pz = [[1 0];[0 -1]];
Sx = Px/2;
Sy = Py/2;
Sz = Pz/2;

x = eye(2^N);

%Calculates spin operators in the Hilbert space of system
for spin = 1:N
        %Superkron neatens tensor product
        spin_ops{1,spin} = kronm(spin_cell(Sx,P0,spin,N),x);
        spin_ops{2,spin} = kronm(spin_cell(Sy,P0,spin,N),x);
        spin_ops{3,spin} = kronm(spin_cell(Sz,P0,spin,N),x);
end

%Defines matrix of hamiltonian
H = zeros(2^N);

j = [1:N 1 2];  %Array of indices to include periodic boundary conditions
%Cycles through each spin and adds the exchange term for NN and NNN
%interactions to the hamiltonian
for i = 1:N
   
   SiSi1 = spin_ops{1,j(i)}*spin_ops{1,j(i+1)}+spin_ops{2,j(i)}*spin_ops{2,j(i+1)}+spin_ops{3,j(i)}*spin_ops{3,j(i+1)};
   SiSi2 = spin_ops{1,j(i)}*spin_ops{1,j(i+2)}+spin_ops{2,j(i)}*spin_ops{2,j(i+2)}+spin_ops{3,j(i)}*spin_ops{3,j(i+2)};
   H = H + SiSi1 + alpha*SiSi2; %NN and NNN exchange terms
   
end

%Calculates eigenvalues of H
[evecs,evals] = eig(H);
disp(min(evals(:)));
toc



%Create cell with each component of the tensor product
%Parameter A: spin operator, B: P0 matrix, pos: index of spin, N: number of spins
function res = spin_cell(A,B,pos,N)
res = cell(1,N);    %Defines cell
    %Cycles through each position in cell
    for position = 1:N
        %Checks whether to insert spin operator or P0
        if position == N+1-pos
            res{position} = A;
        else
            res{position} = B;
        end
    end
end